---
title: 'Set up my dotfiles and base configs'
excerpt: 'About the preparatory work for getting and then setting up my dotfiles.'
---

You have installed all the necessary packages and are now ready to fetch
the configurations and code that makes up my custom desktop session.  It
is assumed you are running the stock MATE session and have an open
terminal.

## The latest fixed release of my dots

The "Code for Prot's Dots For Debian" (Code for PDFD == CPDFD) is the
repository that contains the latest _fixed release_ of my dotfiles.
Such tagged releases are versions that I have tested extensively and am
confident that others can use.  We shall be employing CPDFD, because my
dotfiles' `git` repository is an unstable environment, intended for
running tests and developing new features that eventually end up in
a fixed release.

CPDFD is hosted on GitLab's own instance (gitlab.com).  If you have an
account there and have configured SSH access, you can clone the repo
with the following command:

	git clone git@gitlab.com:protesilaos/cpdfd ~/cpdfd

Otherwise, just clone over HTTPS:

	git clone https://gitlab.com/protesilaos/cpdfd ~/cpdfd

Bear in mind that we clone the repo into the user's home directory.  The
rest of this manual will assume `~/cpdfd` as a constant.

_For the record, my dotfiles are available here:
https://gitlab.com/protesilaos/dotfiles._

## Primer to managing dotfiles with GNU Stow

You will not be copying anything manually.  That is a recipe for
disaster!  Instead we leverage the power of symbolic links, aka
"symlinks", by using GNU Stow.

The way Stow works is to read the filesystem paths defined by a target
and create equivalent symlinks to them at the parent of the present
working directory (or a given destination).  In practice, since `cpdfd`
is in your home directory, all symlinks will be extensions of
`/home/USERNAME/`, else the `~` path.

Let us take a closer look at what it means to give `stow` a target.  In
my dotfiles, I have a directory called "vim".  Its structure looks like
this:

	~/cpdfd $ tree -aF vim
	vim
	├── .vim/
	│   ├── colors/
	│   │   ├── tempus_autumn.vim
	│   │   ├── tempus_classic.vim
	│   │   ├── tempus_dawn.vim
	│   │   ├── tempus_day.vim
	│   │   ├── tempus_dusk.vim
	│   │   ├── tempus_fugit.vim
	│   │   ├── tempus_future.vim
	│   │   ├── tempus_night.vim
	│   │   ├── tempus_past.vim
	│   │   ├── tempus_rift.vim
	│   │   ├── tempus_spring.vim
	│   │   ├── tempus_summer.vim
	│   │   ├── tempus_tempest.vim
	│   │   ├── tempus_totus.vim
	│   │   ├── tempus_warp.vim
	│   │   └── tempus_winter.vim
	│   └── spell/
	│       ├── el.utf-8.spl
	│       ├── el.utf-8.sug
	│       ├── en.utf-8.add
	│       └── en.utf-8.add.spl
	└── .vimrc

	3 directories, 21 files

It includes a `.vimrc` file and a `.vim` directory with more content.
So if you run `stow vim` from within `~/cpdfd` all those paths will be
added to your home directory as symlinks.  Here I also add the `-v`
option for a verbose output:

	~/cpdfd $ stow -v vim
	LINK: .vim => cpdfd/vim/.vim
	LINK: .vimrc => cpdfd/vim/.vimrc

This means:

* `~/.vim` links to `~/cpdfd/vim/.vim`.
* `~/.vimrc` links to `~/cpdfd/vim/.vimrc`.

The contents of `.vim` are accordingly expanded into:

	~/.vim/colors
	~/.vim/colors/tempus_autumn.vim
	~/.vim/colors/tempus_classic.vim
	~/.vim/colors/tempus_dawn.vim
	~/.vim/colors/tempus_day.vim
	~/.vim/colors/tempus_dusk.vim
	~/.vim/colors/tempus_fugit.vim
	~/.vim/colors/tempus_future.vim
	~/.vim/colors/tempus_night.vim
	~/.vim/colors/tempus_past.vim
	~/.vim/colors/tempus_rift.vim
	~/.vim/colors/tempus_spring.vim
	~/.vim/colors/tempus_summer.vim
	~/.vim/colors/tempus_tempest.vim
	~/.vim/colors/tempus_totus.vim
	~/.vim/colors/tempus_warp.vim
	~/.vim/colors/tempus_winter.vim
	~/.vim/spell
	~/.vim/spell/el.utf-8.spl
	~/.vim/spell/el.utf-8.sug
	~/.vim/spell/en.utf-8.add
	~/.vim/spell/en.utf-8.add.spl

From now on, we will be referring to the immediate subdirectories of
`~/cpdfd` as "Stow Packages", as per `man stow`.

Using GNU Stow is essential because many of my Stow Packages contain
somewhat complex structures such as the one shown above.  Plus, keeping
everything symlinked provides the benefit of controlling things with
`git`.  If you need to make changes or pull my latest fixed release, you
will be able to do so centrally at `~/cpdfd`.

## About my Stow packages

Switch to my dotfiles:

	cd ~/cpdfd

Now list only the directories that are relevant for `stow` (they are always
written in lower case letters):

	~/cpdfd $ ls --ignore='[A-Z]*'
	bin  bspwm  colours  compton  dunst  fontconfig  gtk  keyring  music  newsboat  shell  tmux  vim  xterm

Note though, that the directories that are named "NO-STOW-<name>" still
contain useful configs.  However, they need to be manually adapted to
each use case.

Target them all at once (we pass the `-v` flag so that you see where
everything is linked to—though you can always just browse through my
dotfiles' contents):

	stow -v bin bspwm colours compton dunst fontconfig gtk keyring music newsboat shell tmux vim xterm

If Stow throws and error and complains that some files already exist,
you must delete them, rename them, or otherwise move them to another
location.  The common offenders are the default `~/.bashrc` and
`~/.profile` which will block the Stow package called "shell".

**The rest of this book assumes that you have used GNU Stow on all the
appropriate packages, otherwise things will not work as intended.**

Now a few words about each Stow package:

* `bin` contains my custom scripts.  Some of these are an integral part
  of my custom desktop session.  This topic is covered in the chapter
  about my local ~/bin.
* `bspwm` includes the configuration files for the window manager
  (BSPWM) and the hotkey daemon (SXHKD).  The former is where we define
  settings such as the width of the border that is drawn around windows,
  the ratio at which windows are split, and the like.  The hotkey daemon
  stores all the custom key bindings we use to control the session.  For
  more, read the chapter about the basics of my BSPWM.
* `colours` is where my Tempus themes for the shell and X resources are
  located.  These files are used by various scripts of mine and should
  never be edited manually.  For more, read the chapter about the Tempus
  themes.
* `compton` refers to the display compositor and has the relevant config
  file.  We use this to avoid screen tearing, add some subtle shadows
  around windows and enable somewhat smoother transitions.
* `dunst` includes configurations for the daemon that displays desktop
  notifications.  These control the look and feel of the program.
* `fontconfig` includes all of my custom font configurations.  For the
  specifics, refer to the chapter about fonts.
* `gtk` defines the settings for the graphical toolkit.  It also adds
  ports of the Tempus Themes for the GTK3 Source View widget (used by
  text editors such as Gedit, Pluma, Mousepad) as well as the GTK4
  equivalent (used by GNOME Builder).
* `keyring` contains the files necessary for autostarting the GNOME
  Keyring, the tool that stores passwords and secrets.  You can use this
  to store access to SSH keys and the like.
* `music` refers to the configuration files for the Music Player Daemon
  and its ncmpcpp client.  Please note that you need to read the chapter
  about the music setup in order to make this work properly.
* `newsboat` stores the files needed by the RSS/Atom reader of the same
  name.  In order to use this program, you need to add some URLs that
  point to valid feeds.  Read the chapter about Newsboat.
* `shell` defines my Bash-related configurations, including aliases for
  common commands, the command line prompt, and various useful tweaks.
  Read the chapter about my shell setup.
* `tmux` is about the terminal multiplexer used in my default terminal.
  Details are included in the chapter about my Tmux and Vim
  configurations.
* `vim` is my editor of choice, which I use without any plugins or
  whimsical tweaks. As with the above Stow target, refer to the chapter
  about Tmux and Vim.
* `xterm` contains all the necessary files for configuring the terminal
  emulator according to my preference.

## Ready to go!

You are now ready to log in to BSPWM.  Just to be sure, reboot your
system.  When you reach the login screen, look for the drop-down menu on
the top-right corner of the screen, where desktop sessions are listed.
Select `bspwm` and then proceed to add your username and password.

_But before you actually log in, read the following chapter about the
basics of my BSPWM, how to control it and move around, and the like_.

