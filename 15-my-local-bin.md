---
title: 'About my local ~/bin'
excerpt: 'Overview of the custom shell scripts that are bundled with my dotfiles.'
---

An integral part my dotfiles is the `bin` directory.  It includes
various scripts that I have written for use in my custom desktop
session.  Some of these are essential to various functions and
workflows.  Others are convenient extras.

	~/cpdfd $ tree -aF bin
	bin
	├── bin/
	│   ├── bspwm_dynamic_desktops*
	│   ├── bspwm_external_rules*
	│   ├── bspwm_focus_mode*
	│   ├── bspwm_multifaceted_operation*
	│   ├── bspwm_reorder_desktops*
	│   ├── bspwm_smart_move*
	│   ├── bspwm_smart_presel*
	│   ├── clr*
	│   ├── dotsmenu*
	│   ├── flatpakmenu*
	│   ├── gev*
	│   ├── melonpanel*
	│   ├── nbm*
	│   ├── own_script_bspwm_node_resize*
	│   ├── own_script_bspwm_per_host_configs*
	│   ├── own_script_current_keyboard_layout*
	│   ├── own_script_laptop_dual_monitor*
	│   ├── own_script_local_build_tempus_themes*
	│   ├── own_script_mate_setup*
	│   ├── own_script_mate_terminal_setup*
	│   ├── own_script_notify_send_mpc_status*
	│   ├── own_script_print_colour_table*
	│   ├── own_script_run_dmenu_xcolors*
	│   ├── own_script_run_passmenu_xcolors*
	│   ├── own_script_update_environment_theme*
	│   ├── own_script_update_running_terminals*
	│   ├── own_script_update_tmux_running_vim*
	│   ├── passmenu*
	│   ├── poweroptionsmenu*
	│   ├── ptp*
	│   ├── sbg*
	│   ├── sbgmenu*
	│   ├── stm*
	│   ├── stmmenu*
	│   ├── tempus*
	│   ├── tempusmenu*
	│   ├── tmr*
	│   ├── tmux_update_vim*
	│   ├── toggle_compton*
	│   └── toggle_screenkey*
	└── .local/
		└── share/
			└── my_custom_ui_font.sh*

	3 directories, 41 files

Let's first get out of the way the `my_custom_ui_font.sh`.  This file
contains the font definition that is used in my various `dmenu`
implementations.  The code part:

	if [ -n "$(fc-list 'dejavu sans condensed')" ]; then
		my_font_family='DejaVu Sans Condensed'
		my_font_size=12.5
	fi

	my_custom_ui_font="${my_font_family:-sans}-${my_font_size-12}:${my_font_weight:-regular}"

Now a few words about the scripts, with the proviso that you can always
learn more about each one by studying the source code:

* Scripts that start with "own_script" are labelled as such for
  a variety of reasons: (1) their function is ancillary to something
  else, (2) I might develop a better replacement, (3) avoid naming
  conflicts, (4) be descriptive about what each item is about.
* Scripts whose name begins with "bspwm\_" implement various advanced
  functions for the window manager.  These are documented in the chapter
  about the advanced features of my BSPWM.
* In principle, all items have inline documentation, except `passmenu`
  which is provided by another source referenced therein.
* While on the topic of `passmenu`, this is a `dmenu` interface for
  copying to the clipboard a password stored with `pass`.  Such an
  awesome feature!  To put it succinctly, this is what sold me on both
  the power of `dmenu` and the idea of a dedicated password manager that
  works seamlessly with core UNIX utilities.
* Many of these scripts are assigned to custom key bindings.  Better
  check my `sxhkdrc` and/or `sxhkdrc_bspc` to find out the relevant key
  chords (as explained in the chapter about the basics of my BSPWM).
* The items `nbm`, `sbg`, and `sbgmenu` cover various ways of setting
  the desktop backdrop.  I guess it is better explained in this short
  video: [My UNIX-y ways to wallpapers](https://protesilaos.com/codelog/2019-02-09-unix-ways-wallpapers/).
* The `stm` and `stmmenu` are tools for handling my task list, in
  accordance with my own methodology.  They are complementary to each
  other.  This short video of mine should be enough to get you started:
  [UNIX way to task management](https://protesilaos.com/codelog/2019-02-17-unix-ways-todo/).
* `gev` must be run in the terminal to print a table with the status of
  all `git` repositories inside the user's home directory.  Better see
  this video: [Demo of my Git's Eye View](https://protesilaos.com/codelog/2019-03-25-gev-demo/).
* The `own_script_run_*` commands are wrappers around `dmenu_run` and
  `passmenu` respectively.  They are meant to customise the looks to my
  liking, in order to keep things consistent.
* If you have Flatpak applications on your system, `flatpakmenu` will
  provide a nice interface to launching them.
* `melonpanel` is the script that draws the session's panel.  It is what
  provides content/information to `lemonbar` for display on the screen.
  This is discussed at length in the chapter about the top panel.
* Then there are a few items that are meant to integrate my themes into
  the various parts of the running session (as explained in the chapter
  about the Tempus themes): `tempusmenu` is a front end to selecting the
  theme you want; `tempus` does the actual work of applying the new
  theme across all supported programs; running terminals are re-coloured
  with `own_script_update_running_terminals`; while the running Vim
  sessions inside of Tmux are re-styled with the use of
  `tmux_update_vim`.
* `poweroptionsmenu` provides an interface for logging out of the
  session, switching users, locking the screen (using `slock`),
  rebooting, and shutting down the system.  Note that `slock` may have
  an unintuitive interface at first, since all it does is turn the
  screen dark—you need to type your pass word and hit "Return" to unlock
  it.  The screen will switch to a blue colour each time you type.
* `toggle_compton` will enable or disable the display compositor.
  A very useful feature under certain circumstances where running
  `compton` is detrimental to the task at hand.  For instance, I use
  this to shut down the compositor when I am screen casting.
* `toggle_screenkey` is another on/off switch for the program it
  references.
* `ptp` is a script that prints a table with the colour palette of the
  active Tempus theme.  The table is "context-aware", so that it
  displays information depending on the width of the running terminal.
* `clr` calculates the contrast ratio between two colours.  You run it
  by passing two arguments to it, each representing a colour that is
  written in valid hexadecimal RGB notation.
* `tmr` is a very simple timer.  Run it with a single argument that
  represents a valid unit of time in seconds, minutes, hours, such as
  `tmr 10m`.  Once the time has elapsed, it will print a message
  informing you of the time it started and end.  It will also ring an
  alarm for a few seconds.

Finally, it is worth pointing out that the quality of these scripts may
vary considerably.  The newer the script, the better it is.  This is all
part of a learning process as I am not a programmer by trade.  Despite
that, I hope my efforts on this front are enough to set you up and
running on a custom desktop session centred around the Binary Space
Partitioning Window Manager.

