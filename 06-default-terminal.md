---
title: 'About the default terminal setup'
excerpt: 'Information about my default terminal emulator and its configurations.'
---

My default terminal emulator is the standard for the X Window System and
probably the one with the fewest "gotchas": `xterm`.  This has not
always been the case.  Until fairly recently I was using my custom build
of the Simple Terminal, by _suckless_.  Prior to that I was a `urxvt`
user.  I have also worked extensively with `{gnome,mate,xfce4}-terminal`
(VTE-based) and others.

I only tried Xterm last, due to the baseless belief that it was legacy
software, more or less.  This was compounded by the misinformation that
_suckless_ spreads regarding the maintainability of Xterm.  Read what
the main Xterm developer, Thomas E. Dickey, has to say about its many
[look-alikes](https://invisible-island.net/xterm/xterm.faq.html#known_bugs).

Such misconceptions were quickly cast aside once I started reading
through the program's manpage and then checked its upstream provider.
Xterm is a very capable tool that continues to receive regular updates.

To be clear: no terminal is perfect.  Choosing one is a matter of
weighing the pros and cons.  I find that Xterm meets the following
criteria better than its alternatives:

1. Is in the Debian repos.  True for all other options.  However, note
   that `st` (Debian package `stterm`) only makes sense to use as
   a custom build, so it technically does not meet this criterion.
   Overall, Xterm is more suited to Debian than a custom build of the
   Suckless Terminal, because it does not depend on development packages
   and therefore is not going to have any issues over the long term
   (e.g. the upstream has build dependencies that are not available in
   Debian Stable).
2. Is highly configurable and easy to reproduce.  In large part this is
   true for all options though there are differences in degree and ways
   of doing things: `xterm` uses the `.Xresources` file, making it the
   most dotfile-friendly option.
3. Has good performance.  My totally-non-scientific tests suggest that
   only _unpatched_ `st` has a slight edge over Xterm.  But the
   unpatched tool lacks all sorts of features such as scroll-back
   functionality and cursor blinking, meaning that this is not
   a one-to-one comparison.  At any rate, I think the use of `tmux`
   renders this point largely irrelevant.
4. Is agnostic to the choice of desktop environment or toolkit.  True
   for `st` and `urxvt` as well, though not for the VTE-based options.
5. Has good font-drawing capabilities.  This is particularly important
   when using `tmux` and other programs that draw borders.  The
   VTE-based programs have no issues whatsoever.  Xterm requires some
   careful font configurations, which I have taken care of.  `st` can
   only cope well if it is extensively patched.  `urxvt` has weird
   issues with letter spacing.

If you find that `xterm` does not fit your use case, I also have
a script that configures `mate-terminal` to my liking, including colours
that match the rest of the session: `own_script_mate_terminal_setup`.
If you still want to use something else, and are willing to do things
manually, I recommend my custom build of `st` (has no upstream patches
and is configured to use my Tempus themes):
[https://gitlab.com/protesilaos/st](https://gitlab.com/protesilaos/st).

**Be warned that only Xterm is integrated with my dotfiles to keep
things maintainable.**

## The practical uses of Xterm

The many roles of `xterm` are defined in my `sxhkdrc`.  In short:

* Serves as a container for `tmux`, the terminal multiplexer (see
  chapter about my Tmux and Vim setup)
* Can be run on its own.  Offers everything you expect from
  a feature-complete, yet fairly lightweight terminal (though I strongly
  encourage you to learn `tmux`).
* Launches CLI tools (`mutt`, `newsboat`, `ncmpcpp`) in a standalone
  tiled window.
* Runs a console calculator in a standalone floating window.
* Displays the cheat sheet with the most common key chords in
  a standalone floating window.

With these granted, type `super + F1` or `super + Home` to get started
on my `sxhkd` keys (as noted in the chapter about the basics of my
BSPWM).

