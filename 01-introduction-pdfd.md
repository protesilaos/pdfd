---
title: "Introduction to Prot's Dots for Debian"
excerpt: 'PDFD is intended for experienced users of GNU/Linux who are looking for a stable and efficient custom desktop environment.'
---

The purpose of this book is to help you reproduce my custom desktop
session on Debian 10 'Buster' (the current Debian "stable").  The set of
configurations that make up my system shall hereinafter be referred to
as "my dotfiles" or other notions that are described in context.

My dotfiles apply to a BSPWM session.  That tiling window manager is the
core package.  Complementing it, are programs that draw a system panel
at the top of the viewport, display desktop notifications, set the
wallpaper, perform live theme changes to the terminals and graphical
applications, and the like.  The idea is to have a lightweight yet
perfectly functional desktop environment by combining small, specialised
tools.

A short description of my custom desktop session:

* Debian because I prefer longer-term predictability over novelty.
* `bspwm` for fine-grained window management, with my custom scripts for
  added features.
* `tmux` to make the terminal power user even more powerful (no
  plugins).
* `vim` because I need an efficient text editor (no plugins).
* True minimalism: no complex status lines, no fancy prompts, no
  decorative elements that add no functionality whatsoever.
* Carefully-defined font configurations to complement my hardware and
  satisfy my aesthetic preferences.
* Full integration of my Tempus themes for considerate colour contrast
  and easy live theme switching for the entire session.
* The superb Maté desktop environment as a fallback option and provider
  of some important programs and system-wide base configurations.

## A book, not an install.sh

Some of PDFD's chapters or sections thereof can, at times, be read as a shell
script with extensive inline documentation.  While I could have provided a
script to automate the installation process, along with some basic guidelines
on how to use it, I have opted to use this format instead.  It is to ensure
that no random user attempts to install something they do not fully understand,
both in terms of its content and scope.

A book makes the installation process fully transparent to the user.  You can
read what I am doing, inspect the relevant files and, if needed, make changes
on the spot.

By following the steps in _Prot's Dots For Debian_ (PDFD), you will develop an
intimate understanding of my dotfiles, which will help you further customise
the environment to your liking.

This is important and ultimately good for all parties involved, due to
the highly personalised nature of dotfile management.

## Prepare to adapt things to your liking

Please understand that my dotfiles have been designed with my use case
in mind.  There are bits and pieces you might find surplus to your
requirements or outright unsuitable to your needs.  Hopefully this
manual will help you identify them at an early stage and address them
accordingly.

By sharing my work, I do not purport to be an expert on the matter.
I am just a regular user who has spent enough time documenting their
practices.  If you happen to identify areas in my setup that could be
improved or altogether reworked, your contribution shall be taken into
consideration.

## Not for hipsters

The target audience for this book is the **experienced GNU/Linux user
who wants to run Debian 10 'buster' as a desktop OS**.  PDFD may also
appeal to existing BSPWM users of other distros who would like to test
my custom desktop session in a virtual setup or cherry-pick features for
use in their setup.  Newbies are welcome as well, provided they are
willing to learn and understand who this book is for.

You do not need to be a programmer to follow the instructions in PDFD—I
am not one either.  Though it is recommended you have at least some
knowledge of shell scripting: it is required to make certain adaptations
to programs such as the one that controls the system panel.

## Why Debian and not some rolling-release distro?

Exactly because I do not need the latest version of every package ever
released.  I was an Arch Linux user for a good amount of time.  While
I did learn a lot and consider that distro my second favourite, I did
not find any compelling reason to cling on to the rolling-release model
for the entire operating system.

Perhaps paradoxically, Debian is exciting because it is predictable.  It
takes a very conservative stance towards introducing breaking changes.
It does not chase new technology trends for innovation's sake.  Debian
is reliable.  Simple and super effective.

This is precisely what we need for the custom desktop session: a stable
OS that we can expect to remain constant for at least another couple of
years.  Else we are trapped in a state of flux where we need to track
every changelog out there in order to keep our custom desktop
environment in good working condition.

If I were using a rolling-release distro, I would not be writing this
book.  Things change all the time and it is impossible to maintain
documentation such as this: I do not want this to become some full-time
occupation.

Here is a rule that is based on my experience: in an ever-changing OS,
the broader the scope of customisations, the greater the chance of
experiencing breakage or minor annoyances that require manual
interventions.

For someone like myself, who tends to document the various types of
functionality that affect my setup, with this book being a prime
example, it simply is too much trouble to constantly update everything
for whatever marginal benefit there is to be gained from a fresh package
version.

Speaking of "fresh", we should avoid thinking of packages in terms of
groceries.  That is a pernicious metaphor.  Programs remain relevant for
as long as they work and receive security fixes (where appropriate).
The criterion for evaluating a program is not its recency or the hype
around it, but its serviceability.  _"If it ain't broke, don't fix it"_.
Programs that fall in that category do not become "stale" just because
their last upstream release was a few months or years ago.

Stability means predictability and peace-of-mind.  This is the constant
against which one may develop habits to meet their computing
requirements.  Having a stable basis means that you ultimately treat
customisations as a means to the end of a more efficient workflow.  You
do not care about incessant "ricing" per se; constantly tweaking things
for the sake of fitting in with the cool kids.  You are interested in
a custom desktop session that behaves the way you intend.  Every
customisation, every minute refinement, every careful tweak, serves the
purpose of minimising perceived friction, of shortening the distance
between your mind and the machine.  Nothing more, nothing less.

