---
title: 'Per-user MPD setup'
excerpt: 'Instructions to configure the Music Player Daemon on a per-user basis.'
---

The Music Player Daemon is a program that stores information about your
music collection.  It manages metadata and playlists of currently queued
songs or ones that are saved.

MPD uses a server/client model.  To interface with it we need to use one
of the many available client programs.  The simplest one is `mpc`,
a console utility with basic functionality for querying the daemon's
status, handling media controls, and the like.  A more feature-rich
client is `ncmpcpp`, which also runs in the console.

## Basic configuration

MPD may be run as a system-wide server and can be controlled remotely as
well.  For my use case, all I need is a local, user-specific setup.
This is what we will be doing.

Assuming you have followed the instructions in the chapter about
installing the core packages, you should have these in place:

	sudo apt install mpd mpc ncmpcpp mpdris2 python-mutagen playerctl

Then switch to the base of my dotfiles' directory and proceed to create
symlinks for the music-related programs:

	~/cpdfd $ stow music

Now you need to run a series of commands to configure `mpd` in
accordance with our per-user requirements.  Start by disabling the
`systemd` service, since we autostart `mpd` from within the `bspwm`
session:

	sudo systemctl disable mpd

Now switch to the local config directory:

	cd ~/.config/mpd

Create the directory where saved playlist data is stored:

	mkdir playlists

Generate the files `mpd` needs in order to run:

	touch database log pid state sticker.sql

Configuration is done!  You might need to reboot for changes to take
effect.

## Update the database

To update the `mpd` database (assuming the presence of audio files at
`~/Music`) either run `mpc update` in a terminal or type `ncmpcpp` and
then press `u`.  If your music is in a different directory, edit the
relevant path in `~/.config/mpd/mpd.conf`.

## Basics of ncmpcpp

The "Ncurses Console Media Player C++".  To start interfacing with this
excellent tool, simply type `ncmpcpp` in a terminal, or use the key
chord chain `super + x ; 5` (see the chapter about the basics of my
BSPWM).

To play music, learn how to use `ncmpcpp` by studying the corresponding
manpage.  I typically switch to screen 4, by hitting `4`, then `A` and
hit enter.  This inserts an empty prompt which adds all available music
to the playlist.  Then I toggle on repeat mode with `r`, random playback
order with `z`, and a 5 second cross-fade with `x`.

If you do not like `ncmpcpp`, I highly recommend `cantata`, a graphical
MPD front end using the Qt toolkit which, however, does not integrate
well with my mostly GTK- and text- based environment (as such, Cantata
is not part of my custom desktop session).

## Why we also need mpc

The comparatively simpler `mpc` tool performs the role of a remote
control.  We use it to assign key chords with which to control `mpd`
even when no client is running.  In a similar fashion, `mpc` provides
a simple way of querying the status of the server for the sake of, say,
displaying information about the current song on the panel (see the
chapter about the top panel).

## The MPRIS bridge

By default, `mpd` does not behave like most up-to-date music players in
the GNU/Linux world.  Put differently, it cannot be controlled by
dedicated media keys, nor interact with specialised tools that might be
offered by the desktop environment.

This kind of functionality is part of the MPRIS protocol.  To make `mpd`
a good citizen, we have fetched the `mpdris2` package as well as
a dedicated, console-based program for controlling MPRIS-aware
applications: `playerctl`.

With these in place, the Music Player Daemon (and all other MPRIS-aware
media players, like the ever-popular `vlc`) can be controlled using the
keyboard's dedicated media keys.  See `man playerctl` for more demanding
types of interaction.  The key chords are defined in my `sxhkdrc` as
explained in the chapter about the basics of my BSPWM.

## Peace of mind

MPD is great because while it is powerful and can cater to the needs of
the most avid audiophile, it also works seamlessly once you set it up.
I prefer it over its alternatives because it is very much _laissez
faire_, to the point you almost forget it is even there.

When you log back in to the custom BSPWM session, `mpd` will be up and
running, waiting for your input in a paused state (or stopped, in case
that was the last interaction with it).

You do not need to keep any window open, nor run some resource-hungry
web app just for the sake of playing audio from your local collection.
Use the keyboard's dedicated key for play/pause or use the combination
`super + down arrow`: music will start playing, while you get on with
your work (do not forget to see my `sxhkdrc` for the relevant key
chords).

