---
title: 'The Tempus themes'
excerpt: 'Tempus is a collection of accessible themes for Vim and terminal emulators.  It is part of my custom desktop session.'
---

PRIOR NOTICE: all the scripts referenced herein are discussed in greater
detail in the chapter about my local `~/bin`.

* * *

All the colours you see are part of a project of mine called [Tempus
themes](https://protesilaos.com//tempus-themes/).  These are colour
schemes for Vim, terminal emulators and relevant tools.  They consist of
sixteen hues that correspond to the standard escape sequences for
terminal colour definitions, in order from 0 to 15:

	0-7: black, red, green, yellow, blue, magenta, cyan, white
	8-15: bright {black, red, green, yellow, blue, magenta, cyan, white}

I have designed Tempus with the purpose of filling a perceived void in
the theming space: that of accessible, functional, yet still presentable
colour schemes.  Tempus is designed to work well for as many users as
possible, myself included.

In more technical terms, the Tempus themes are compliant **at minimum**
with the _Web Content Accessibility Guidelines_ (WCAG) level AA for
colour contrast between foreground and background values.  This stands
for a relative luminance ratio of 4.5:1.  That ratio is but a threshold
as some members of the collection conform with the even higher standard
of 7:1 (WCAG AAA).

## Accessibility is about good design

To dispel any misconceptions, "accessibility" is not only useful for
a subset of users.  Accessibility is another way of describing
thoughtful design.  It benefits everyone.  Being forced to read light
grey on a white background or green text on a red backdrop is a major
failure, indicating a lack of forethought and empathy with the user.

For us who spend considerable amounts of time interfacing with
a computer, a good contrast ratio is essential to a subconsciously
pleasant session (just as with good typography, as discussed in the
chapter about fonts).  Whether you read, write, code, your eyes will
appreciate the improved legibility of text on the screen.

Try this: miscalibrate your monitor and/or your font configurations to
make text look blurry and irritating to the eye.  Work in that
environment for a while to realise how much more painful it is.  Eye
strain is real, so do yourself a service by prioritising legibility over
whimsy.

If you are used to inaccessible colour combinations, you may find Tempus
themes a bit too sharp at first.  Give it some time: your eyes will
adapt to the improved presentation.  I know from experience, as I used
to work with inaccessible themes.  In fact, I had developed an entire
collection of them: [Prot16](https://protesilaos.com/schemes/).  Those
were not designed with accessibility and functionality in mind.  Some
might even "look better" than the Tempus themes, but none _works
better_.

## The Tempus themes collection

As of this writing (2019-07-02) the Tempus themes collection consists of
the following items (WCAG rating in parentheses):

	# Light themes
	dawn       (AA)
	day        (AA)
	fugit      (AA)
	past       (AA)
	totus      (AAA)

	# Dark themes
	autumn     (AA)
	classic    (AA)
	dusk       (AA)
	future     (AAA)
	night      (AAA)
	rift       (AA)
	spring     (AA)
	summer     (AA)
	tempest    (AAA)
	warp       (AA)
	winter     (AA)

The one you get by default is Tempus Classic, which is a dark theme with
warm hues.

## Tempus and my dotfiles

With the exception of GTK widget and icon themes, Tempus is applied
everywhere colours can be defined.  Some notable examples are `bspwm`,
`vim`, `tmux`, `newsboat`, `neomutt`, `dunst`, and my `lemonbar` script
called `melonpanel`.  Even GTK-based text editors are covered, such as
`gedit` (GNOME default), `pluma` (MATE default), and `gnome-builder`.

A shell script is responsible for changing themes across all those
applications: it is aptly named `tempus`, which is a _backronym_
standing for "Themes Every Meticulous Person Ultimately Seeks" (in case
you do not know, "tempus" is a real word in Latin).

You can run `tempus` directly from the terminal by adding a scheme's
unique name as its argument.  For example, to switch to the light theme
`day`, you would do:

	tempus day

A slightly more convenient way is to use `tempusmenu`.  This is
a `dmenu` script that interfaces with the aforementioned command.  It is
bound to a hotkey sequence for your convenience.  To invoke
`tempusmenu`, type `super + e  ; t` (hold `super` and press `e`, then
release `super` and just press `t`—this is a key chord chain, as
discussed in the chapter about the basics of my BSPWM).  A menu with all
available items will appear.  Either type your choice and press enter or
use the arrow keys (or CLI/Emacs motions) and press enter.

My dotfiles are designed in such a way that the following changes will
occur when performing a theme switch across the entire environment:

* All running terminals will "live reload" their colours using escape
  sequences.  Thanks to my `own_script_update_running_terminals`.
  Similarly, all running CLI programs like `vim`, `newsboat`, `neomutt`,
  `less` will gracefully adjust to the theme change.
* My default terminal emulator, `xterm`, will also write the new colour
  definitions to its configuration file (also see the chapter about the
  default terminal).
* The terminal multiplexer (`tmux`) will be instructed to reload its
  configurations in order to parse the new colours.
* The top panel (`melonpanel`) will be re-run and display the new
  colours.  Same with the notification daemon (`dunst`).
* All running GTK applications, including Flatpaks, will convert to
  a light or dark variant of their current theme, depending on whether
  the selected Tempus theme is light or dark (assuming you followed the
  instructions in the chapter about installing core packages).  So if
  you choose `totus`, for example, which is dark text on a light
  background, the GTK themes will also be light, and vice versa.  This
  is courtesy of a running settings daemon.  If you have followed the
  instructions in the chapters about installing Debian 10 'buster' and
  adding the core packages, this is the `mate-settings-daemon`.
* The wallpaper will change accordingly, if you have defined images at
  `~/Pictures/theme/{dark,light}.jpg`.

All this happens in the blink of an eye.  If you care about the
specifics (or wish to contribute) you need to examine the script that
does the heavy lifting: `tempus`.

In my opinion, this is a very neat feature.  Having it bound to a key
chord makes it even better.  Now if you need any convincing of why you
should ever be able to switch themes on the fly, try coding using a dark
theme under direct sunlight…

## Additional resources

The Tempus themes is a standalone project that happens to be integrated
with my dotfiles.  There are a number of `git` repos.  The specialised
ones are specific to the program they reference:

* [Tempus themes](https://gitlab.com/protesilaos/tempus-themes).  The
  main content hub, which provides information about the project, screen
  shots, and offers all available ports in a central place.
* [Tempus themes GTK3 Source
  View](https://gitlab.com/protesilaos/tempus-themes-gtksourceview3).
  For GTK-based text editors like Gedit, Pluma, Mousepad.
* [Tempus themes GTK4 Source
  View](https://gitlab.com/protesilaos/tempus-themes-gtksourceview4).
  Only known implementation is for GNOME builder though other apps will
  follow once the migration to GTK4 gains momentum.
* [Tempus themes Kitty](https://gitlab.com/protesilaos/tempus-themes-kitty).
* [Tempus themes Konsole](https://gitlab.com/protesilaos/tempus-themes-konsole).
* [Tempus themes ST](https://gitlab.com/protesilaos/tempus-themes-st).
* [Tempus themes Tilix](https://gitlab.com/protesilaos/tempus-themes-tilix).
* [Tempus themes URxvt](https://gitlab.com/protesilaos/tempus-themes-urxvt).
* [Tempus themes Vim](https://gitlab.com/protesilaos/tempus-themes-vim).
* [Tempus themes Xfce4-terminal](https://gitlab.com/protesilaos/tempus-themes-xfce4-terminal).
* [Tempus themes Xterm](https://gitlab.com/protesilaos/tempus-themes-xterm).

Also see the [Tempus themes
Generator](https://gitlab.com/protesilaos/tempus-themes-generator): the
tool that produces all these ports and the place where you could
contribute new templates, code, ideas.

## Known issues

The "live reloading" of all running instances of `vim` is contingent on
`tmux` (see chapter on my Tmux+Vim combo).  If you are running the text
editor outside of the multiplexer's control (e.g. a GUI), then the theme
switch requires manual intervention.  Either exit Vim and enter again or
run `:source ~/.vimrc`.  To fix this, I would need a program that can
send key chords to running applications: this is possible within `tmux`,
as can be seen in `tmux_update_vim`.  There is `xdotool`, but my tests
did not yield promising results…

If you are using the `mate-terminal` (recall that `xterm` is my default
and recommended option) and try to open a new instance of it while the
theme switch is underway, it will likely retain its older colour scheme
in the database, even if it might catch the step where terminals are
live reloaded via escape sequences.  In such a case the theme switch
needs to be performed anew or you must manually run
`own_script_mate_terminal_setup`.

