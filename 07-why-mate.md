---
title: 'Why choose the MATE Desktop'
excerpt: 'The reasons for choosing MATE as the default desktop environment for the system.'
---

You should already be up-and-running with the custom desktop session.
But there is still much to learn from the rest of this book.  Let us
review the choice of MATE (Maté) as the default option for the desktop
environment.

In the chapter about installing Debian 10 'buster' on your machine,
I suggested you select MATE as a core part of your new system.  The
installer's major component selection should look like this:

	[x] Debian desktop environment
	[ ] ... GNOME
	[ ] ... Xfce
	[ ] ... KDE Plasma
	[ ] ... Cinnamon
	[x] ... MATE
	[ ] ... LXDE
	[ ] ... LXQt
	[ ] web server
	[ ] print server
	[x] SSH server
	[x] standard system utilities

You may be wondering why I recommend this step: _is not the purpose of
this manual to set up a custom BSPWM session?_  The reasons are
basically these:

1. You get all the Xorg and GTK dependencies in place.  You need these
   anyhow for running a graphical session and some of the most common
   application software, such as Firefox and Thunderbird.
2. You install some MATE programs that my custom desktop session makes
   use of.  These are `caja` (file manager) and `mate-settings-daemon`
   (touched upon in the chapter about the Tempus themes).
3. You have a robust fallback option in case you need to use the
   computer without BSPWM (e.g. multiple users on a single computer).
4. Things like networking, the policy kit agent, and the xdg backend
   (matching programs to MIME types) are configured for you.

## What about minimalism?

Minimalism is about striving to achieve a state of minimum viability,
the least possible completeness, but not less.  We still want something
that works without any "gotchas" (edge cases where things do not perform
at the desired standard).

Installing a fallback DE at the very outset makes things simpler, more
predictable and maintainable over the entire life-cycle of the Debian
system; a system that can span multiple releases of Debian's "stable"
distribution.

Do not be a nag about some Internet meme.

* Bloat is when you run a modified web browser as your text editor, or
  whatever the latest trend is with Electron-based applications and web
  apps.
* Bloat is when your favourite web page eats up 50% of my CPU.
* Bloat is Vim with a spaghetti code of plugins and concomitant settings
  that try to reinvent the wheel.
* Bloat is a much-vaunted 'minimalist' program like `st` with
  a hodgepodge of patches piled on top of it.
* Bloat is every addition that actively detracts from the end goal of
  usability, such as fancy animations for every interaction, wanton use
  of transparency and blur effects, etc.

Free software is about choices.  Best we keep our opinions to ourselves
and do what we consider appropriate for our particular use case.  Enough
with the misbegotten elitism and the smugness that certain communities
actively cultivate.

To my eyes, setting up Xorg is a task that falls outside the scope of
dotfile management.  Same with configuring the network stack and similar
basic utilities.  The moment we enter into that domain we start creating
our own distro or bespoke sysadmin setup.  Let Debian handle that, while
we focus on stowing the dotfiles in place, so that we may get our
desired workflow going.

Personally, I do not keep track of the package count to determine the
'bloat' installed on my machine.  This is not a game where the user with
the fewer packages wins.  If a piece of software is genuinely useful and
is known to be fairly stable and maintained, then I keep it.

## Why specifically MATE and not "foo"?

MATE is the most complete GTK-based desktop environment after GNOME,
_without using the GNOME shell stack_ (in which case we should mention
Cinnamon and Budgie).  This is to be expected due to its heritage: it is
the fork and subsequent continuation of the GNOME 2 code base.

MATE's main apps are very competent tools.  The file manager, _Caja_, is
a good piece of software.  The document viewer, _Atril_, gets the job
done.  Same with the image viewer, _Eye of MATE_.  Meanwhile, the
archive manager, _Engrampa_, will handle compressing or decompressing
data without any problem.

Make no mistake: MATE is far from perfect.  Otherwise it would be
frivolous to set up a _custom_ desktop session.

Compared to the other options provided by the Debian installer:

* MATE is ideal for a stable distro such as Debian 10 (same with Xfce).
  It is developed at a rather slow yet steady pace, perfectly
  complementing the stability and predictability of the underlying OS.
  Debian is a poor fit for tracking DEs like GNOME and KDE Plasma that
  are developed at a somewhat fast pace.  For instance, the GNOME
  version in 'buster' (3.30) is already one release behind upstream and
  will be two by the end of summer 2019.  The gap will continue to widen
  every six months, _ceteris paribus_.  Similar story with KDE Plasma.
* MATE is fairly modular, much like Xfce.  You can use its individual
  components without having to run the entire session.  And unlike Xfce,
  MATE has already completed the migration to GTK3 (last checked on
  2019-07-01).
* Unlike GNOME (GNOME 3), MATE is committed to preserving the
  traditional desktop metaphor instead of turning the desktop into an
  oversized phone UI.  It also is less taxing on system resources.
* Compared to Xfce, MATE does not need to pull in packages from another
  DE in order to function properly.  Whereas Xfce lacks its own archive
  manager, document viewer, calculator, among others.
* MATE is more lightweight than GNOME and at least on par with—if not
  lighter than—KDE Plasma, without losing out on any of the core
  functionality.  For our use case, being light-yet-complete and
  self-contained is exactly what we are looking for.

My point is clear: MATE is the best GTK-based option we could go for.

