---
title: 'Notes about my shell setup'
excerpt: 'Things to consider about my bashrc and related configurations.'
---

I only use `bash` as my CLI shell.  It is ubiquitous.  It works.  And,
because I have no need for fancy extras or technology previews, my
shell's setup has no plugins, external add-ons, or other obscure
extensions (same as with my Vim and Tmux, by the way).

	~/cpdfd $ tree -aF shell
	shell
	├── .bash_profile
	├── .bashrc
	├── .inputrc
	├── .local/
	│   └── share/
	│       └── my_bash/
	│           └── dircolors
	├── .profile
	└── .xsessionrc

There are a couple of files that are only read at startup: `.xsessionrc`
and `.profile`.  The former exists for the sole purpose of sourcing the
latter.  In `.profile` I have instructions to:

* Use my `.bashrc`.
* Add `~/bin` to the `PATH`.
* Launch the keyring (mostly for storing SSH credentials).

`.bash_profile` is also just a placeholder for sourcing the `.bashrc`.
Meanwhile, `.inputrc` provides some basic options for slightly modifying
the behaviour of the command line interpreter.  These concern the (1)
reduction of key presses for tab completion, (2) textual and colourised
information about files and directories, (3) colours that enhance the
visuals of tab-completion's feedback.

## About the .bashrc

As is the norm with my dotfiles, the `.bashrc` is heavily documented.
This is an overview of its headline options:

* `PAGER` and `MANPAGER` is `less` with the option to quit after
  reaching the end of a file and trying to read further below.
* The `EDITOR` is `vim`, while its GUI variant is the `VISUAL`.
* The default browser is whatever is defined by the MIME list (should be
  `firefox-esr`).
* The prompt is simple and super effective: `<filesystem path> $ ` by
  default or, if running via an SSH connection, it becomes slightly more
  informative with `<user>@<host>: <filesystem path> $ `.
* Enable tab-completion when starting commands with `sudo`.

Then I have a comprehensive list of aliases, that you may or may not
like to keep in place.  Run `alias` from the command prompt to get the
full list and/or filter its output accordingly.  Here is a sample:

	~ $ alias | grep 'apt'
	alias aac='sudo apt autoclean'
	alias aar='sudo apt autoremove -V'
	alias adl='apt download'
	alias afu='sudo apt full-upgrade -V'
	alias ai='sudo apt install'
	alias air='sudo apt install --reinstall'
	alias ali='apt list --installed'
	alias alu='apt list --upgradable'
	alias ama='sudo apt-mark auto'
	alias amm='sudo apt-mark manual'
	alias apc='sudo aptitude purge ~c'
	alias ar='sudo apt remove -V'
	alias ard='apt rdepends'
	alias as='apt search'
	alias ash='apt show'
	alias au='sudo apt update'
	alias aug='sudo apt upgrade -V'
	alias aulu='sudo apt update && apt list --upgradable'
	alias auu='sudo apt update && sudo apt upgrade -V'
	alias auufu='sudo apt update && sudo apt upgrade -V && sudo apt full-upgrade -V'

The most opinionated aliases are those that change the default behaviour
of core utilities like `cp`, `mv`, `rm`.  They make their output verbose
and introduce a prompt for confirming user input where appropriate.
I believe these are sound defaults, as they protect you from accidents.
That granted, you can always run an unmodified command by prepending
a backslash `\`.  For example:

	# This uses the alias for `cp`
	~/cpdfd $ cp README test
	'README' -> 'test'
	~/cpdfd $

	# This uses the original `cp`
	~/cpdfd $ \cp README test
	~/cpdfd $

Finally, there are a few functions:

* `man()` configures `man` to show some colours and formatting for the
  various parts of its syntax.
* `cd()` tells `cd` to list in a clean way the directory's contents when
  entering it, including the hidden items, though not the implicit ones.
* `backupthis()` can be run as `backupthis <file>` to create a copy of
  the target, with a time stamp as its extension and identifier.

That just about covers it.  I wish you luck on your PATH; make sure to
get HOME safe.

