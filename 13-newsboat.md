---
title: 'Quick guide to set up Newsboat'
excerpt: 'How to read RSS/Atom feeds in the terminal.'
---

Part of my customisations concern `newsboat`, a decent RSS/Atom reader
for the console.  If you have never heard what an RSS or Atom feed is,
it is a data stream that is meant to be read by client programs, known
as "readers" or "feed readers".  Whenever the source that publishes the
feed makes a new entry (publishes something), the stream is populated
with the new content, which the reader can then fetch.  This makes it
possible to keep track of multiple websites from a single interface.
Having all that in the terminal makes things even better.

	~/cpdfd $ tree -aF newsboat/
	newsboat/
	└── .config/
		└── newsboat/
			├── config
			├── queue
			├── themes/
			│   ├── tempus_theme_dark.theme
			│   └── tempus_theme_light.theme
			└── urls -> /path/to/my/symlink # not part of the dotfiles

	3 directories, 5 files

All you need to get started is to first put things in place:

	~/cpdfd $ stow -v newsboat
	LINK: .config/newsboat => ../dotfiles/newsboat/.config/newsboat

Then to actually read some feeds, you must _manually create_ a `urls`
file at `~/.config/newsboat/` with a single feed source per line, like
this:

	https://www.debian.org/News/news
	https://bits.debian.org/feeds/atom.xml
	https://www.gnu.org/software/guix/news/feed.xml

I personally **exclude this file from the dotfiles because it might
contain sensitive information**.

If you want to filter feeds, you can add one or more tags to them, with
a space demarcating each entry:

	https://www.debian.org/News/news "Distro"
	https://bits.debian.org/feeds/atom.xml "Distro Community"
	https://www.gnu.org/software/guix/news/feed.xml "GNU Linux Distro"

Tags are used to aggregate content from multiple sources:

	# A catch-all filter for unread items
	"query:Unread Articles:unread = \"yes\""

	# Some tag-specific filters
	"query:GNU plus Linux:tags # \"Distro\""
	"query:ARBITRARY NAME OF FILTER:tags # \"Tag\""

Once you launch the program, either from the command line or with the
key chord chain `super + x ; 4` (see chapter on the basics of BSPWM) you
will start seeing news items pop up, grouped by source URL.  From there
you can use Vi motions or the arrow keys to select the desired item and
either read it in place by hitting the `Return` key (`Enter`) or open it
in the browser by hitting `o` (`firefox-esr` is the default and what
I use).

The config file is fairly simple, with each setting usually describing
what it does:

	# General
	# -------
	show-read-articles no
	show-read-feeds no
	prepopulate-query-feeds no
	feed-sort-order unreadarticlecount
	show-keymap-hint yes
	swap-title-and-hints no
	text-width 72
	save-path "~/Documents/archived-articles/rss"
	browser /usr/bin/xdg-open %u
	confirm-exit yes
	display-article-progress yes

For more, see the source for the actual configurations.  Also read the
manual with `man newsboat`.

