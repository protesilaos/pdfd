---
title: 'Installing Debian 10'
excerpt: 'Essential information for installing Debian on your machine'
---

To get Debian 10 'buster' on our machine, we are going to use one of the
netinstall iso images.  The official way is to follow the standard
method, which includes only free/libre software, or to use the
unconventional method which comes preconfigured with the `contrib` and
`non-free` package repos.  The latter is intended for awkward hardware
setups that absolutely require certain non-free packages (firmware
drivers) to run the installer.  Here are the corresponding links:

* [Debian 10 netinstall](https://cdimage.debian.org/cdimage/release/current/amd64/iso-cd/)
* [Debian 10 netinstall (non-free firmware)](https://cdimage.debian.org/cdimage/unofficial/non-free/images-including-firmware/current/amd64/iso-cd/)

## Short note about free software

Debian refers to the second iso as "unofficial".  Do not let that
mislead you.  It is still provided by the team responsible for the
installer images.  In this context, "unofficial" means that it does not
fully conform with Debian's Free Software Guidelines.

While understandable, this is a rather unconvincing attempt to maintain
the line that Debian only ships with free/libre software.  I can, thus,
see why the Free Software Foundation or the GNU project[^GNUViewDistros]
do not include Debian in their list of approved distributions that do
not sacrifice software freedom for convenience.[^FSFDistros]

Personally, I use Debian with a single non-free package that is
necessary to enable my Wi-Fi card.  Otherwise I could not run a free OS
_at all_.

If you start with the unofficial method, you can still opt to run only
free software by retroactively removing any "contrib" and "non-free"
entries from the APT sources list.  System administration of this sort
is outside the scope of PDFD.  I have, nonetheless, taken care to only
recommend libre software in the pages of this book.

## Writing the latest release iso

Politics aside, let us proceed with the installation.  You need to
verify your iso with information provided by Debian (from the pages you
get the iso from).  Once the checks are done, write the iso to a flash
drive.  I usually follow these steps after I insert the medium and open
a new terminal:

	# prints information about devices
	sudo fdisk -l

	# the flash drive I insterted is usually at /dev/sdb
	# unmount the flash drive
	umount /dev/sdb

	# write to it
	sudo dd if=/path/to/iso/file of=/dev/sdb

	# eject the flash drive
	sudo eject /dev/sdb

**Please be extemely careful with the above commands**, especially when
identifying the flash drive.  Pay attention to the output of `sudo fdisk
-l` and make sure you correctly spot the writeable medium you intend to
use.  Carelessness might result in data loss or other damages.

## The installation process

Now on to get Debian on the machine.  Insert the flash drive and power
on the computer.  You are given the choice of a simple graphical or
textual interface, as well as advanced options.  If in doubt, go with
the graphical option.  Once the installer starts, you will have to
choose your language and keyboard settings, set your root user's
password, create a regular user, and the like.

At some point in the installation process, you will be asked to select
your major system components.  These include a Desktop Environment, an
SSH server, a print server, and a web server.  I always keep the first
option checked, then using the space key to toggle on/off I add MATE,
SSH server, remove the print server, and keep the standard system
utilities.

The selection screen looks like this:

	[x] Debian desktop environment
	[ ] ... GNOME
	[ ] ... Xfce
	[ ] ... KDE Plasma
	[ ] ... Cinnamon
	[x] ... MATE
	[ ] ... LXDE
	[ ] ... LXQt
	[ ] web server
	[ ] print server
	[x] SSH server
	[x] standard system utilities

You can omit the SSH server if you have no use for it.  Follow the
remaining steps and after a while you will have successfully installed
Debian on your machine.  Congratulations!

Later in the book I explain why you should also choose MATE.  For the
time being, let us proceed to the next chapter of actually installing
the core packages and configuring things accordingly.

[^GNUViewDistros]: [Explaining Why We Don't Endorse Other Systems](https://www.gnu.org/distros/common-distros.html).  By the GNU project.

[^FSFDistros]: [Free GNU/Linux distributions](https://www.gnu.org/distros/free-distros.html).  By the GNU project.

